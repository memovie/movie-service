package id.phully.movie.controllers;

import id.phully.movie.models.movies.*;
import id.phully.movie.repositories.GenreRepository;
import id.phully.movie.repositories.MovieRepository;
import id.phully.movie.utilities.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/public")
public class GenreController {

    @Autowired
    GenreRepository genreRepository;

    @Autowired
    MovieRepository movieRepository;

    @GetMapping({"/genres"})
    public Page<Genre> index(Pageable pageable) {
        return genreRepository.findAll(pageable);
    }

    @GetMapping({"/genres/{genresQueryTitle}"})
    public Page<Movie> show(
            @PathVariable String genresQueryTitle,
            Pageable pageable
    ) {
        if (genreRepository.existsByNameQuery(genresQueryTitle)) {
            return movieRepository.findAllByGenres_NameQuery(genresQueryTitle, pageable);
        } else {
            throw new ResourceNotFoundException();
        }
    }

}
