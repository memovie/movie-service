package id.phully.movie.controllers;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import id.phully.movie.models.comments.Comment;
import id.phully.movie.models.movies.Movie;
import id.phully.movie.repositories.MovieRepository;
import id.phully.movie.services.CommentService;
import id.phully.movie.utilities.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/public")
public class MovieController {

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    CommentService commentService;

    @GetMapping({"/movies"})
    public Page<Movie> index(Pageable pageable) {
        return movieRepository.findAll(pageable);
    }

    @HystrixProperty(name = "hystrix.command.default.execution.timeout.enabled", value = "false")
    @GetMapping({"/movies/{moviesQueryTitle}"})
    public Map<String, Object> findMovieDetail(
            @PathVariable String moviesQueryTitle
    ) {
        if (movieRepository.existsByTitleQuery(moviesQueryTitle)){
            Map<String, Object> movieComment = new HashMap<>();
            Movie movie = movieRepository.findByTitleQuery(moviesQueryTitle);
            movieComment.put("movie", movie);
            movieComment.put("comment", commentService.getAllComment(movie.getId()));
            return movieComment;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping({"/getmovies"})
    public Map<String, Object> finmoviead() {
        List<Comment> comment =  commentService.getAllComment("54f1e975-578c-4b01-aa19-84b305308841");
        Map<String, Object> movieComment = new HashMap<>();
        movieComment.put("movie", comment);
        return movieComment;
    }

}
