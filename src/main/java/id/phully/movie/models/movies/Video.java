package id.phully.movie.models.movies;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.phully.movie.models.audit.Audit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "videos")
@Setter
@Getter
@NoArgsConstructor
public class Video extends Audit {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @Size(max = 255)
    private String videoUrl;

    @NotNull
    @Size(max = 255)
    private String videoQuality;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "movie_id", nullable = false)
    @JsonIgnore
    private Movie movie;


    public Video(@NotNull @Size(max = 255) String videoUrl, @NotNull @Size(max = 255) String videoQuality) {
        this.videoUrl = videoUrl;
        this.videoQuality = videoQuality;
    }
}
