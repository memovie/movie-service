package id.phully.movie.models.movies;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.phully.movie.models.audit.Audit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "actors")
@Setter
@Getter
@NoArgsConstructor
public class Actor extends Audit {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @Size(max = 255)
    @Column(unique = true)
    private String name;

    @NotNull
    @Size(max = 255)
    @Column(unique = true)
    private String nameQuery;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "actors")
    @JsonIgnore
    private Set<Movie> movies = new HashSet<>();

    public Actor(@NotNull @Size(max = 255) String name, @NotNull @Size(max = 255) String nameQuery) {
        this.name = name;
        this.nameQuery = nameQuery;
    }
}
