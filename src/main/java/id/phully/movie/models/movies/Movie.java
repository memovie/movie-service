package id.phully.movie.models.movies;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.phully.movie.models.audit.Audit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "movies")
@Setter
@Getter
@NoArgsConstructor
public class Movie extends Audit {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @Size(max = 255)
    @Column(unique = true)
    private String title;

    @NotNull
    @Size(max = 255)
    @Column(unique = true)
    private String titleQuery;

    @NotNull
    @Size(max = 1000)
    private String cover;

    @NotNull
    @Size(max = 255)
    private String thumbnail;

    @NotNull
    @Size(max = 255)
    private String rating;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "movie_genre",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "genre_id")})
    @JsonIgnoreProperties("movies")
    private Set<Genre> genres = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "movie_actor",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "actor_id")})
    private Set<Actor> actors = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "movie")
    private Set<Video> videos = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "movie_director",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "director_id")})
    @JsonIgnoreProperties("movies")
    private Set<Director> directors = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "movie_production",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "production_id")})
    @JsonIgnoreProperties("movies")
    private Set<Production> productions = new HashSet<>();

    @NotNull
    private int releaseDate;

    @Size(max = 255)
    private String country;

    @NotNull
    @Size(max = 255)
    private String movieStatus;

    @NotNull
    private int movieDuration;

    public Movie(@NotNull @Size(max = 255) String title, @NotNull @Size(max = 255) String titleQuery, @NotNull @Size(max = 255) String cover, @NotNull @Size(max = 50) String thumbnail, @NotNull @Size(max = 50) String rating, @NotNull int releaseDate, @Size(max = 255) String country, @NotNull @Size(max = 255) String movieStatus, @NotNull int movieDuration) {
        this.title = title;
        this.titleQuery = titleQuery;
        this.cover = cover;
        this.thumbnail = thumbnail;
        this.rating = rating;
        this.releaseDate = releaseDate;
        this.country = country;
        this.movieStatus = movieStatus;
        this.movieDuration = movieDuration;
    }
}

