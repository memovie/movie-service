package id.phully.movie.models.comments;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Field;

@Setter
@Getter
@NoArgsConstructor
public class Comment {

    private String _id;
    @Field("user")
    private User user;
    private String post;
    private String type;
    private String message;

    public Comment(String _id, User user, String post, String type, String message) {
        this._id = _id;
        this.user = user;
        this.post = post;
        this.type = type;
        this.message = message;
    }
}
