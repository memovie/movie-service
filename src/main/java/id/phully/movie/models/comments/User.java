package id.phully.movie.models.comments;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class User {

    private String email;
    private String fullName;
    private String avatar;

    public User(String email, String fullName, String avatar) {
        this.email = email;
        this.fullName = fullName;
        this.avatar = avatar;
    }
}
