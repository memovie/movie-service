package id.phully.movie.services;

import id.phully.movie.models.comments.Comment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@FeignClient(value = "comment-service", fallback = CommentService.CatalogFallback.class)
public interface CommentService {

    @RequestMapping(method = RequestMethod.GET, value = "public/comment/{postId}", consumes = "application/json")
    List<Comment> getAllComment(@PathVariable("postId") String postId);

    @Component
    class CatalogFallback implements CommentService{
        @Override
        public List<Comment> getAllComment(String id) {
            return null;
        }
    }


}
