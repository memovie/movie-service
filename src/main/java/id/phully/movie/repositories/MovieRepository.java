package id.phully.movie.repositories;

import id.phully.movie.models.movies.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MovieRepository extends JpaRepository<Movie, String> {
    Movie findByTitleQuery(String titleQuery);

    Page<Movie> findAllByGenres_NameQuery(String genres_Name, Pageable pageable);

    Boolean existsByTitleQuery(String titleQuery);
}
