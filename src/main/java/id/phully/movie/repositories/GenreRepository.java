package id.phully.movie.repositories;

import id.phully.movie.models.movies.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenreRepository extends JpaRepository<Genre, String> {
    Boolean existsByNameQuery(String genresNameTitleQuery);
}
