package id.phully.movie.repositories;

import id.phully.movie.models.movies.Production;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductionRepository extends JpaRepository<Production, String> {
}
