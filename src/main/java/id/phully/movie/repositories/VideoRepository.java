package id.phully.movie.repositories;

import id.phully.movie.models.movies.Video;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VideoRepository extends JpaRepository<Video, String> {
}
