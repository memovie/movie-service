package id.phully.movie.repositories;

import id.phully.movie.models.movies.Director;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DirectorRepository extends JpaRepository<Director, String> {
}
